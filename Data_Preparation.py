import pandas as pd

# Draftees
drafteesName = 'draftees.csv'

# NBA Stats (WS) DONE
nbaDataName = 'brdata_nodupes.csv'

# College Stats ()
ncaaDataName = 'brcdata.csv'

def createDataset(draftees, nbaDf, ncaaDf):
    addedRowsIndex = 0
    outputDf = pd.DataFrame()
    for index,row in draftees.iterrows():
        tempDf = nbaDf[nbaDf['Player'].str.lower().str.contains(row['Name'].lower())]
        if not tempDf.empty:
            tempDf = tempDf[['Player', 'WS', 'MP']]
            nbaResult = tempDf.groupby('Player', as_index=False).mean()
            tempDf = ncaaDf[ncaaDf['Player'].str.lower().str.contains(row['Name'].lower())]
            if not tempDf.empty:
                ncaaResult = tempDf.drop(tempDf.index.to_list()[1:], axis=0)
                ncaaResult.insert(2, "NBA WS", [nbaResult.iloc[0]['WS']], True)
                ncaaResult.insert(2, "NBA MP", [nbaResult.iloc[0]['MP']], True)
                if addedRowsIndex == 0:
                    outputDf = ncaaResult
                else:
                    outputDf = pd.concat([outputDf, ncaaResult], axis=0)
                addedRowsIndex += 1

                print(ncaaResult)
            else:
                print("No college data")
        else:
            print("No NBA data")

    print(outputDf)
    outputDf.to_csv('output.csv')
    return None

def main():
    dc2019df = pd.read_csv(drafteesName)
    ncaaDatadf = pd.read_csv(ncaaDataName)
    nbaDatadf = pd.read_csv(nbaDataName)

    createDataset(dc2019df,nbaDatadf,ncaaDatadf)

if __name__ == "__main__":
    main()