import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import Ridge
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

df = pd.read_csv('output.csv', header=1)

non_relevant_columns = ['Player', 'Class', 'Season', 'Pos', 'School', 'Conf']
df_numeric = df.drop(non_relevant_columns, axis=1)

df_numeric['FGA.1'] = pd.to_numeric(df_numeric['FGA.1'], errors='coerce')
df_numeric_filled = df_numeric.fillna(df_numeric.mean())

X = df_numeric_filled.drop('NBA WS', axis=1)
y = df_numeric_filled['NBA WS']

scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)

X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.2, random_state=42)

ridge_reg = Ridge(alpha=1.0)

ridge_reg.fit(X_train, y_train)

y_pred = ridge_reg.predict(X_test)
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)
mae = mean_absolute_error(y_test, y_pred)
rss = np.sum((y_test - y_pred) ** 2)

print("Mean Squared Error:", mse)
print("R-squared:", r2)
print("Mean Absolute Error:", mae)
print("Residual Sum of Squares:", rss)

# Regression Plot: Actual vs. Predicted Values
plt.figure(figsize=(10, 6))
sns.regplot(x=y_test, y=y_pred, ci=None, color="b")
plt.xlabel('Actual Values')
plt.ylabel('Predicted Values')
plt.title('Actual vs. Predicted Values')
plt.savefig('regression_plot_ridge.png')  
plt.close() 

# Residual Plot
residuals = y_test - y_pred
plt.figure(figsize=(10, 6))
sns.scatterplot(x=y_test, y=residuals)
plt.xlabel('Actual Values')
plt.ylabel('Residuals')
plt.title('Residual Plot')
plt.axhline(y=0, color='red', linestyle='--')
plt.savefig('residual_plot_ridge.png') 
plt.close()  

# Coefficient Plot
coefficients = ridge_reg.coef_
plt.figure(figsize=(12, 8))  # Adjusted for better readability
feature_names = X.columns
sns.barplot(x=feature_names, y=coefficients)
plt.xticks(rotation=90)  # Rotate feature names for better visibility
plt.xlabel('Features')
plt.ylabel('Coefficient Magnitude')
plt.title('Feature Coefficients')
plt.tight_layout()  # Adjust layout to make room for the rotated x-axis labels
plt.savefig('coefficient_plot_ridge.png') 
plt.close()  
